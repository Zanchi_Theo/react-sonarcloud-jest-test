import Link from 'next/link'
import Header from '../components/Header'
import { PropLayout } from '../components/layout'
import AboutPageContent from '../components/aboutpagecontent'

const About = () => {
    return (
        <PropLayout content={AboutPageContent} />
    )
}

export default About